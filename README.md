# Carte agronomique du département de Seinte-et-Marne

## Build the app
You need to have `npm` or `yarn` installed to build the application. To do so run:
```sh
$ yarn install 
# OU
$ npm install
```

## Start the app
```sh
$ yarn start
# OU
$ npm run start
```

## Build map tiles
### Prequisites
In order to generate map's tiles you need to have `GDAL` installed.
```sh
# Debian-based OS
$ sudo apt-get install python-gdal
# Fedora/RedHat 
$ sudo dnf install gdal gdal-devel gdal-python
```

Then you have to install the GDAL with PIP:

```
pip install GDAL
``` 
If if fails, like on my setup (fedora distro), here's the procedure I followed:
```sh
# 1. check the version installed
$ gdal-config --version # prints 2.1.3
$ pip download GDAL==2.1.3
$ tar -xf GDAL-2.1.3.tar.gz 
$ cd GDAL-2.1.3
$ python setup.py build_ext --include-dirs=/var/include/gdal
$ python setup.py install
```

### Generate tiles
Raw HD image must be placed under the `raw_map/raw.png` file. 
Then to generate tiles simply run:
```sh
$ npm run build:tiles
# OR
$ yarn build:tiles
```
