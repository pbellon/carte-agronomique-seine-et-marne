import React, { Component } from 'react'

import { Map, TileLayer } from 'react-leaflet'
import AboutModal from '../AboutModal';
import { latLng, latLngBounds } from 'leaflet';

import './styles.css';

const getTilesURL = () => {
    const is_production = process.env.NODE_ENV !== 'development'
    const base_url = is_production ? 'https://maptiles-c74d.kxcdn.com' : (
        process.env.PUBLIC_URL || (window ? window.location.href : '')
    );
    return `${base_url}/tiles/{z}/{x}/{y}.png`;
}

class CustomMap extends Component {
  render(){
    const tiles_url = getTilesURL()
    console.log('tiles URL', tiles_url);
    const attribution = [
        '<a href="http://1886.u-bordeaux-montaigne.fr" target="_blank" rel="nofollow">Université Bordeaux Montaigne - 1886</a>',
        'Delesse, Achille Ernest Oscar Joseph 1817-1881',
        'Carte agronomique du département de Seine-et-Marne',
        '17-10-07-04',
        '<a href="http://1886.u-bordeaux-montaigne.fr/items/show/70477">document</a>',
    ].join('&nbsp;-&nbsp;')
    const bounds = latLngBounds(
      latLng(100, -200.0), latLng(-100, 90)
    )
    return (
      <Map
        center={[45, 10]}
        zoom={3}
        maxZoom={7}
        maxBounds={ bounds }
      >
      <AboutModal/>
      <TileLayer
        bounds={bounds}
        url={tiles_url}
        attribution={attribution}
      />
      </Map>
    );
  }
}

export default CustomMap;
