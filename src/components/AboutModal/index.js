import React, { Component } from 'react';

import './styles.css';
import Modal from '../Modal';
import ReactMarkdown from 'react-markdown';

const md = `
# Carte agronomique du département Seine-et-Marne (1843-1878)
Numérisation d'une ancienne carte de Achille Ernest Deselle représentant le territoire de Seinte et Marne au travers du prisme Agronomique. On y voit les différentes cultures du département ainsi que les revenus associés.

## Crédits
- Conception & Développement: [Pierre Bellon](https://twitter.com/toutenrab)

## Cartes utilisées
La carte affichée est un assemblage réalisé à partir de deux cartes issues de la cartothèque [1886 de l'Université Bordeaux Montaigne][1886].

1. Delesse, Achille Ernest Oscar Joseph 1817-1881  
Carte agronomique du département de Seine-et-Marne - [http://1886.u-bordeaux-montaigne.fr/items/show/70476](http://1886.u-bordeaux-montaigne.fr/items/show/70476)
2. Delesse, Achille Ernest Oscar Joseph 1817-1881  
Carte agronomique du département de Seine-et-Marne - [http://1886.u-bordeaux-montaigne.fr/items/show/70477](http://1886.u-bordeaux-montaigne.fr/items/show/70477) 

## Librairies utilisées
- React
- Leaflet
- Génération de tiles: gdal2tiles

## Inspiration
- navigae.fr

[1886]: http://1886.u-bordeaux-montaigne.fr
`;

class AboutModal extends Component {
  static defaultProps = {
    isOpen: false,
  }
  constructor(props){
    super(props);
    this.state = {
      isOpen: false,
    }
  }
  closeModal(e){
    this.setState({ isOpen: false });
  }
 
  openModal(){
    this.setState({ isOpen: true });
  }

  render(){
    const { isOpen } = this.state;
    const modalKlass = 'modal' + (isOpen ? ' modal--opened' : '');
    return (
      
      <div className={ 'button-holder' }>
        <div className={ 'about-button' } onClick={ this.openModal.bind(this) }>?</div>
        <Modal isOpen={ isOpen }>
          <div className={ modalKlass }>
            <div className={ 'modal-background' } onClick={ this.closeModal.bind(this) }></div>
            <div className={ 'modal-content' }>
              <div className={'modal-content-container' }>
                <div className={ 'modal-close-button '} onClick={ this.closeModal.bind(this) }>&#215;</div>
                <ReactMarkdown source={ md }/>
              </div>
            </div>
          </div>
        </Modal>
      </div>
    );
  }

}
export default AboutModal;
